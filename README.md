# Slides template

## Description

This repository allows you to easily deploy slides to expose them to a set of persons or to the public using [reveal.js](http://lab.hakim.se/reveal-js/#/), [jekyll](https://jekyllrb.com/), [staticrypt](https://github.com/robinmoisson/staticrypt) & [Gitlab CI](https://about.gitlab.com/features/gitlab-ci-cd/).

## Usage

### Fork

- Fork me
- :warning: In **Settings**/**General**, remove the fork relationship! :warning:
- Change my **repo name**, **description** and **logo**
- Activate a runner to enable CI Pipelines

### Dependencies

- [Install Jekyll](https://jekyllrb.com/docs/installation/)
- Pull `reveal.js` submodule with `git submodule update --init`

### Getting started

- Edit `_config.yml` (especially title) to customize your slides
- Create your slides in `_posts`
- `jekyll serve` to watch them in real time

⚠️ `_site/index.html` is to be gitted for simplicity's sake on facultative authentication!

### Passord protecting

Once you're done, if you need to password-protect your pages, go [here](https://fhuitelec.gitlab.io/html-encryptor/) and follow the instructions.

Be sure to add a **page title** and **instructions** before generating the new HTML page.

Download the HTML file with password prompt and paste it in `_site/index.html`

To try the password prompt:

```
jekyll serve --skip-initial-build
```

Commit and push and you're done!
